(описание задания ниже)

## Описание работы приложения ##

Приложение состоит из библиотек *Parser.pm* и *DATA.pm*, а так же скрипта *main.pl*.

* **DATA.pm** - класс чтения данных из файлов. По заданию, это не требуется, но для тестов было необходимо получать данные блоками не более 500мб. Класс реализует функцию аналогичную абстрактной функции FOO::GetDataChunk() из задания.

* **Parser.pm** - библиотека приобразования выражений в текствовом виде в результат.

* * **shunting_yard($str)** - преобразование выражения в текстовом виде в форму Обратной Польской Нотации\*. Преобразование происходит согласно алгоритму Сортировочной Станции\*\*

*  * **rpn_result($expr)** - калькулятор данных в форме ОПН. Возвращает результат выражения

*  * **work** - функция-воркер для пула заданий по преобразованию выражения в результат

```
#!perl

sub work {
     return (rpn_result( shunting_yard($str)));
}

```

**main.pl** - скрипт логики. Считывает данные объемом до 500мб и анализирует их. Т.к. неизвестно, будут ли поступать данные полными блоками, выделяем из потока неполное начало блока, неполный конец блока и целиковые данные из середины. Все сложенные или полные выражения отправляются на обработку в пул воркеров. Выражения считаются параллельно с тем, как поступают и анализируются входные данные. Воркер складывает результат в хэш вида:

```
#!perl

хеш->{блок}->{имя} = результат;
```

После того, как закончатся все входные данные и будут обработаны все вырожения, результат выводится на экран в алфовитном порядке.


### Примечание ###

* \* Обратная Польская Нотация - https://en.wikipedia.org/wiki/Reverse_Polish_notation

* \*\* Алгоритм Сортировочной Станции - http://en.wikipedia.org/wiki/Shunting-yard_algorithm


====================================

# Тестовое задание
 
Имеем некоторый набор телиметрии, полученный от черного ящика (исходные данные).

Формат входных данных определен следующими условиями:

- Входной файл представляет собой набор блоков.

- Блок определен операторами START и END, которые определяют начало и конец блока соответственно, и содержит набор заданий для вычисления, разделенных между собой точкой с запятой.

- Оператор выделен пятью дефисами до и после.

- Задание начинается с названия уникальной для этого блока переменной, а дальше, после знака присвоения «:=», следует простое математическое выражение.

- Математическое выражение может содержать операторы сложения «+», вычитания «-», умножения «x», и деления «:». Все операнды в таких выражениях представляют собой численные значения. Кроме того возможно использование скобок.

### Пример блока входных данных:
 

```
#!perl

-----START <Block Name> ----- Var1:=(324,456 + 896,942.31 :92.54) * 1:2;

Var2 := 894783456.21+20:-2----- END <Block Name>-----
```


### Задание: ###

1. Написать программу для вычисления результатов заданий с использованием языка perl.

2. Входные данные можно получить функцией Foo::GetDataChunk() из абстрактной внешней библиотеки. Функция вернет ссылку на строковый скаляр с куском входных данных. Размер куска никак не контроллируется и может занимать до 500Мб, хотя это и маловероятно...

3. Входные данные генерируются неизвестным внешним источником, потому могут содержать опечатки, ошибки или даже code injection, помните об этом 

4. Результат выполнения программы должен быть выведен в алфавитном порядке, по одной записи на строку, каждая строка результата имеет следующий формат: Section.Variable = Value;

5. Вы можете использовать файл small.dat для проверки работы своей программы

### Оценка работы:
 
1. Скорость работы программы (а также другие показатели производительности). 

2. Лаконичность кода. Естественно заменять пробелы на табы не нужно, оцениваться будет не размер файла, а его содержимое на предмет перегруженности бесполезными конструкциями (или их отсутствия)

3. Читаемость кода. Не стремитесь писать код по общепринятому «стандарту», у нас принят свой Code Guideline, пишите как Вам удобно, но Ваш код должен быть легко понятным, удобно читаемым и красиво оформленным.

4. Корректность выдаваемого результата.

5. Для оценки работы программы будет использоваться файл huge.dat, размером 5Гб
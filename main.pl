#!/usr/bin/perl -w

use strict;
use warnings;
use utf8;
use feature qw(say);
use classes::DATA;
use AnyEvent::Fork::Pool;
use AnyEvent::Fork;

#для межпроц. взаимодействия
my $cv = AnyEvent->condvar;
#количество ядер в системе
my $cpus = AnyEvent::Fork::Pool::ncpu 1;
#будем считать формулы во время того, как файлы еще считываются
my $pool = AnyEvent::Fork
	->new
	->require ('classes::Parser')
	->AnyEvent::Fork::Pool::run(
		"Parser::work",         # Модуль::Функция - рабочая функция воркера
		max  => $cpus,          # Количество воркеров в пуле
		idle => 16,              # Количество воркеров при простое
		load => 32,              # Размер очереди воркера
		on_destroy => (my $finish = AnyEvent::condvar), # Вызов при уничтожении объекта
	);

my $current = undef;
my $ending = undef;
my $query = {};

#по условия задания, данные поступают через абстрактный класс Foo функцию GetDataChunk(). Данные неизвестного размера до 500мб возвращаются в виде ссылки на скаляр. Для тестовых целей был реализован класс DATA для собственных попыток получить нужные данные. Параметром можно передать количество возвращаемых байт
my $file = new DATA(file => 'small.dat', length => '256');

#побежали по входным данным. Получаемые данные приходят случайного размера
while (my $block = $file->GetDataChunk(int(rand(536870912)))){

	$$block =~ s/[\s\n]//g;#удалим все пробелы и переносы строк	

	#возможно, что блоки будут разбиты и их надо будет собрать сложив следующую или предыдущую итерацию.
	#заберем кусочек неполного начала (если есть)
	#регексп ищиет, если НЕ '^-----STARTLegmen-----' или 'Digret := 3 + 4'
	unless (!defined $ending && $$block =~ /^(?:\-{5}START(\w+)\-{5}|[A-Z]\w+\:\=)/) {
		$$block =~ /^(.*?)(?:\-{5}END\w+\-{5}|\;)/; #берем начало до END или до ;	
		if (defined $1 && defined $ending) { 
			# собираем обрезаный  кусочек
			my $full = $ending.$1;
			$full =~ /(?:START(\w+)\-{5}|)(\w+)\:\=(.*?)$/;
			$current = $1 if defined $1;
			#добавляем задание на расчет в пул воркеров
			$pool->( $3,$current,$2, sub {
				my ($current, $name, $result) = @_;
				$query->{$current}->{$name} = $result;
				$cv->send($query);
			});
			$ending = undef;
			$full = undef;
		} 	
		else{$ending .= $$block; next}; #в блоке нет начало или конца, перенесем эти данные в следующую итерацию
	}

	$current = $1 if defined $1;# получить названия блока из регекспа, если он есть

	#заберем кусочек с конца (если есть)
	unless ($$block =~ /(\-{5}END\w+\-{5}|\;)$/) { # -----ENDLegmen----- или ;
		$$block =~ /(?:\-{5}START\w+\-{5}(?:.*\;+)*|.*\;+|^)(.*)$/; # -----STARTLegmen----- .* ; или просто .* или начало строки ;	
		$ending = $1 if defined $1; #недефайнет может быть если в этом блоке нет начала. тогда значение сюда добавится в блоке с получением первой части (см.  выше)
	}

	#заберем все, что есть целое по центру
	while ($$block =~ /(?:\-{5}START(\w+)\-{5}|\;+|^)([A-Z]\w+)\:\=(.*?)(?=\;)/g) { # (STARTLegmen----- или ;)(Digreg:= 3 + 4);
		$current = $1 if defined $1;
		my $name = $2;
		#добавляем задание на расчет в пул воркеров
		$pool->( $3,$current,$2, sub {
			my ($current, $name, $result) = @_;
			$query->{$current}->{$name} = $result;
			$cv->send($query);
		});
	}
}

#ждем завершения воркеров и получаем от них данне
undef $pool;
$finish->recv;
$query = $cv->recv;

#выводим всё на экран в отсортированном алфавитном порядке
foreach my $key (sort keys %{$query}){
	foreach my $name (sort keys %{$query->{$key}}){
		say $key.".".$name."=".$query->{$key}->{$name}.";";
	}
}

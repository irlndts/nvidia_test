package DATA;

use Moose;

has 'file' => (
	is => 'rw',
	writer => 'setFile',
	reader => 'getFile',
	);

has 'offset' => (
	is => 'rw',
	writer => 'setOffset',
	reader => 'getOffset',
	default => 0,
	);

has 'length' => (
	is => 'rw',
	writer => 'setLength',
	reader => 'getLength',
	);

#по условия задания, данные поступают через абстрактный класс Foo функцию GetDataChunk(). Данные неизвестного размера до 500мб возвращаются в виде ссылки на скаляр. Для тестовых целей был реализован класс DATA для собственных попыток получить нужные данные. Параметром можно передать количество возвращаемых байт. По умолчанию это значение length.
sub GetDataChunk{
	my $self = shift;
	
	my $offset = $self->getOffset();

	my $length = shift || $self->getLength();

	open (FILE, "<", $self->getFile()) or die "Can't open file";
	binmode(FILE);
	
	seek (FILE, $self->getOffset(),0);

	my $block = undef;
	my $output = read (FILE, $block, $length); #считать нужный блок информации
	$self->setOffset($self->getOffset() + $length); #отодвинуть поинтер в еще непрочитанное место файла
	
	close (FILE);

	return $output?\$block:0; #если файл пустой, вернуть 0. Если нет, то вернуть считанную информацию
	
}



1;

package Parser;
use feature qw(say);
use strict;

#WHAT
#функции и алгоритмы для парсинга метематических выражений


#воркер, который будет вызываться для пула задач
sub work {
	my $str = shift;
	my $current = shift;
	my $name = shift;
	return ($current, $name ,rpn_result( shunting_yard($str)));
}

my %prec = (
		'-u'=> 7,
		'^' => 6, # в данной задаче не требуется
		'x' => 5,
		':' => 5,
		'+' => 2,
		'-' => 2,
		'(' => 1,
		'' => 9,
	   );

my %right=('-u'=>1);

#что делать с той или иной операцией
my $dispatch_table = {
	'+' => sub {
		my @stack = @_;
		push(@stack,(pop(@stack) + pop(@stack)));
		return @stack;
	},
	'-' => sub { 
		my @stack = @_;
		push(@stack, - (pop(@stack) - pop(@stack)));
		return @stack;
	},
	'x' => sub { 
		my @stack = @_;
		push(@stack,(pop(@stack) * pop(@stack)));
		return @stack;
	},

	':' => sub { 
		my @stack = @_;
		my ($a,$b) = (pop(@stack),pop(@stack));
		return ("Division by Zero",undef) if ($a eq 0); #если предполагается деление на ноль - вернуть ошибку
		push(@stack,($b/$a));
		return (undef,@stack);
	},

};


#реализация калькулятора обратной польской нотации
#на входе выражение разделеное пробелами
#на выходе результат
sub rpn_result{
	my ($expr,$error) = @_;
	return $error unless $expr; #если была ошибка - пробросить ошибку
	my @tokens = split(" ",$expr);
	my @stack;
	foreach (@tokens){
		if (/^\d+(?:\,\d{3})*(?:\.\d+){0,1}$/) { $_ =~ s/\,//g; push(@stack,$_);}
		elsif (/^\:/){($error, @stack) = $dispatch_table->{':'}->(@stack); return $error if defined $error } #при делениее возможно деление на ноль. Если такое произошло - вернем ошибку
		elsif (defined $dispatch_table->{$_}){ @stack = $dispatch_table->{$_}->(@stack);} #все операции, кроме деления
		elsif (/^_1\*$/) { push(@stack,(pop(@stack) * -1));} # если значение отрицательно
		else { return "Unknown value: $_";};
	}
	return pop(@stack);
}

#алгоритм сортировочной станции для разбора математического выражения 
#на входе строка с выражением
#на выходие разобранная строка или какая-то ошибка
#линейная сложность O(n)
sub shunting_yard{
	my ($str) = @_;

	#получить все элементы отбросив мусор
	my @tokens = grep {! m/^$/} split (/\ *([\+\-\/\(\)]|\d+(?:\,\d{3})*(?:\.\d+){0,1}) */, $str); #regexp - символы и числы типа 1 или 1,000 или 1,000.01 и тп
	$, = " ";

	my $result = undef;
	my @operators_stack;
	my @output;

	my $last="";
	foreach my $token (@tokens) {
		if($token eq '-' and getprec($last)>=0) {$token='-u';}
		if($token=~/^\d+(?:\,\d{3})*(?:\.\d+){0,1}$/) { #числовые значени в т.ч. типа float 
			if($last eq $token || $last eq ")") {
				return (0, "Value tokens must be separated by an operator");
			}
			push(@output, $token); #кладем значение в левую часть
		} elsif($token eq '(') {
			push(@operators_stack, $token);
		} elsif($token eq ')') {
			while(scalar @operators_stack && $operators_stack[-1] ne '(') { #сбрасываем все значения между ( ) в левую часть
				push(@output, pop(@operators_stack));
			}
			unless (scalar @operators_stack && pop(@operators_stack) eq '(') #скобка больше не нужна, удаляем её
			{
				return (0, "Can't find (");
			};
		} elsif((my $pr=getprec($token))>0) { #сортируем токеныё
			if(exists $right{$token}) {
				push(@output, pop(@operators_stack)) while(scalar @operators_stack>0 && $pr<getprec($operators_stack[-1]));
			} else {
				push (@output, pop(@operators_stack)) while(scalar @operators_stack>0 && $pr<=getprec($operators_stack[-1])); 
			}
			push(@operators_stack, $token);
		} else {
			return (0,"Unknown token: \"$token\"");
		}
		$last=$token;
	}
	# достигли конца входной строки, собираем левую и правую часть воедино 
	push(@output, pop(@operators_stack)) while(scalar @operators_stack>0); 

	foreach my $token (@output) { #готовим ответ, если значение -N, то минус обозначаем как _1*
		if($token eq '-u') {$result .= '_1* ';}
		else {$result .= "$token ";}
	}
	return $result;
}

sub getprec {
	my ($op)=@_;
	return exists $prec{$op}?$prec{$op}:-1;
}

1;
